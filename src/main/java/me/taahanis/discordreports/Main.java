/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.discordreports;

import javax.security.auth.login.LoginException;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author moh_a
 */
public class Main extends JavaPlugin {
    
    public static Main plugin;
    public JDA jda;
    
    @Override
    public void onEnable()
    {
        if (!getDataFolder().exists())
        {
            getDataFolder().mkdirs();
        }
        getConfig().options().copyDefaults(true);
        saveConfig();
        if (getConfig().getString("discord.token").isEmpty())
        {
            Bukkit.getLogger().info("Disabling DiscordReports due to no token for bot has been provided.");
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        try {
            loadJDA();
        } catch (LoginException e) {
            e.printStackTrace();
        }
        getCommand("report").setExecutor(new CommandReport());
        Bukkit.getLogger().info("DiscordReports is now enabling!");
    }
    
    @Override
    public void onDisable()
    {
        
    }
    
    public void loadJDA() throws LoginException
    {
        jda = new JDABuilder(AccountType.BOT)
                .setToken(getConfig().getString("discord.token"))
                .buildAsync();
    }

    public JDA getJDA()
    {
        return jda;
    }
    
    
}
