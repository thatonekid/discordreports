package me.taahanis.discordreports;

import net.dv8tion.jda.core.EmbedBuilder;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.awt.*;

public class CommandReport implements CommandExecutor {

    public Main plugin = Main.getPlugin(Main.class);

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Ingame command only [DiscordReports]");
            return true;
        }

        if (args.length == 0)
        {
            sender.sendMessage(ChatColor.RED + "Correct usage is: /report <player> <reason>");
            return true;
        }
        Player player = (Player) sender;
        if (!player.hasPermission("discordreports.make"))
        {
            player.sendMessage(ChatColor.AQUA + "No permission!");
            return true;
        }
        Player target = Bukkit.getServer().getPlayer(args[0]);
        String reason = StringUtils.join(ArrayUtils.subarray(args, 1, args.length), " ");
        if (target == null)
        {
            sender.sendMessage(ChatColor.GOLD + "Player not found!");
            return true;
        }
        if (reason.isEmpty())
        {
            sender.sendMessage(ChatColor.RED + "You must provide a reason!");
            return true;
        }

        player.sendMessage(ChatColor.GREEN + "Your report has been sent to the staff team.");
        EmbedBuilder eb = new EmbedBuilder();
        eb.setColor(Color.GREEN);
        eb.setTitle("Game Reports");
        eb.addField("Reported User", target.getName(), true);
        eb.addField("Reported By", player.getName(), true);
        eb.addField("Reason", reason, false);
        plugin.getJDA().getGuildById(plugin.getConfig().getString("discord.guildID")).getTextChannelById(plugin.getConfig().getString("discord.reportsID")).sendMessage(eb.build()).queue();
        for (Player players : Bukkit.getOnlinePlayers())
        {
            if (players.hasPermission("discordreports.view"))
            {
                String report = plugin.getConfig().getString("reports.message");
                report = ChatColor.translateAlternateColorCodes('&', report);
                report = report.replace("%player%", target.getName());
                report = report.replace("%sender%", sender.getName());
                report = report.replace("%reason%", reason);
                //String report = ChatColor.RED + player.getName() + " has made a report upon " + target.getName() + " for '" + reason + "' at " + player.getLocation().getX() + ", " + player.getLocation().getY() + ", " + player.getLocation().getZ();
                players.sendMessage(report);
                return true;
            }

        }

        return true;
    }
}
